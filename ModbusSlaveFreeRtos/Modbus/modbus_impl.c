

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "stdio.h"
#include <string.h>
#include "usart.h"
#include <stdint.h>
#include "gpio.h"
#include "modbus_slave.h"
#include "modbus_impl.h"


void Modbus_WriteSingleRegister(uint16_t startAddress, uint16_t value)
{
		printf("writing to single register address %x : %x\n" , startAddress, value);
}

void Modbus_WriteSingleCoil(uint16_t startAddress, uint16_t value)
{
	
	if (value == 0)
	{
			printf("writing to single coil 0 address %x : %x\n" , startAddress, value);	  
	}
	else if (value == 0xFF00)		
	{
			printf("writing to single coil 1 address %x : %x\n" , startAddress, value);	  
	}
	else if (value == 1)		
	{
			printf("writing to single coil 1 address %x : %x\n" , startAddress, value);	  
	}	
}
