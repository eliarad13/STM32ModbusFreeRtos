
/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "stdio.h"
#include <string.h>
#include "usart.h"
#include <stdint.h>
#include "gpio.h"
#include "modbus_funcs.h"
#include "modbus_slave.h"
#include "modbus_impl.h"

uint8_t response[MAX_MB_SLAVE_FRAME_BUFFER];

static uint16_t CalcCRC(uint8_t *buf, int size);


static uint16_t crcTable[] = {
        	0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
        	0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
        	0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
        	0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
        	0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
        	0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
        	0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
        	0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
        	0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
        	0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
        	0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
        	0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
        	0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
        	0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
        	0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
        	0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
        	0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
        	0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
        	0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
        	0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
        	0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
        	0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
        	0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
        	0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
        	0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
        	0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
        	0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
        	0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
        	0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
        	0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
        	0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
        	0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 
        };


// static variables 
static bool DataStore_CoilDiscretes[100];
static bool DataStore_InputDiscretes[100];
static uint16_t DataStore_HoldingRegisters[125];
static uint16_t DataStore_InputRegisters[125];

void MB_DataStoreInit(void)
{

	  // This is only for debug purposes
	  for (int i = 0 ; i < 125 ; i++)
		{
			DataStore_HoldingRegisters[i] = i ;
			DataStore_InputRegisters[i] = i;
		}
		
}	

int MB_Apply_ReadCoilDiscretes(uint8_t *frame)
{
		return 0;
}
int MB_Apply_ReadInputDiscretes(uint8_t *frame)
{
		return 0;
}

 
// 6.4 04 (0x04) Read Input Registers
int MB_Apply_ReadInputRegisters(uint8_t *frame)
{

	
	#pragma pack(push, 1)
	typedef struct 
	{
		byte SlaveAddress;
		byte Function;
		byte StartingAddressHi;
	  byte StartingAddressLo;
	  byte QuantityRegistersHi;
	  byte QuantityRegistersLo;
	  byte CRCLo;
	  byte CRCHi;			
		
	} ST_RHoldingRegisters; 
	#pragma pack(pop)	
	
	ST_RHoldingRegisters  *s = (ST_RHoldingRegisters *) frame;
	
	uint16_t Address = (s->StartingAddressHi << 8) | s->StartingAddressLo - 1;
	uint16_t RegCount =   (s->QuantityRegistersHi << 8)   |  s->QuantityRegistersLo;
  uint16_t txCRC = (s->CRCHi << 8)   |  s->CRCLo;
	printf("txCRC = %x\n" , txCRC);
	
	uint16_t crc = CalcCRC(frame, 6);
	printf("incoming crc = 0x%x\n", crc);
	if (crc != txCRC)
	{
		 printf("incorrect incoming CRC %x  %x\n" , txCRC, crc);
	}	
	
	printf("reading holding register address %x : %d\n" , Address, RegCount);
	
	response[0] = frame[0];  // slave address
	response[1] = frame[1];  // function code 
	response[2] = RegCount * 2;  // byte count
	
	
	// here we response back the data.
	
	int j = 0;
	for (int i = 0 ; i < RegCount * 2; i+=2)
	{
		 response[ 3 + i ] = (DataStore_InputRegisters[Address + j] & 0xFF00) >> 8;
		 response[ 3 + i + 1] = DataStore_InputRegisters[Address + j] & 0xFF;
		 j++;
	}
	
	crc = CalcCRC(response, 3 + RegCount * 2);
	printf("outgoing crc = 0x%x\n", crc);
	
	response[3 + RegCount * 2] = crc & 0xFF ;
	response[3 + RegCount * 2 + 1] = (crc >> 8) & 0xFF;			
	
	return 3 + RegCount * 2 + 2;
}

// see 03 (0x03) Read Holding Registers
int MB_Apply_ReadHoldingRegisters(uint8_t *frame)
{

	
	#pragma pack(push, 1)
	typedef struct 
	{
		byte SlaveAddress;
		byte Function;
		byte StartingAddressHi;
	  byte StartingAddressLo;
	  byte QuantityRegistersHi;
	  byte QuantityRegistersLo;
		byte CRCLo;
	  byte CRCHi;	
		
	} ST_RHoldingRegisters; 
	#pragma pack(pop)	
	
	ST_RHoldingRegisters  *s = (ST_RHoldingRegisters *) frame;
	
	uint16_t Address = (s->StartingAddressHi << 8) | s->StartingAddressLo - 1;
	uint16_t RegCount = (s->QuantityRegistersHi << 8)   |  s->QuantityRegistersLo;
	uint16_t txCRC = (s->CRCHi << 8)   |  s->CRCLo;
	printf("txCRC = %x\n" , txCRC);
	
	uint16_t crc = CalcCRC(frame, 6);
	printf("incoming crc = 0x%x\n", crc);
	if (crc != txCRC)
	{
		 printf("incorrect incoming CRC %x  %x\n" , txCRC, crc);
	}
	
	printf("reading holding register address %x : %d\n" , Address, RegCount);
	
	response[0] = frame[0];  // slave address
	response[1] = frame[1];  // function code 
	response[2] = RegCount * 2;  // byte count
	
	
	// here we response back the data.
	
	int j = 0;
	for (int i = 0 ; i < RegCount * 2; i+=2)
	{
		 response[ 3 + i ] = (DataStore_HoldingRegisters[Address + j] & 0xFF00) >> 8;
		 response[ 3 + i + 1] = DataStore_HoldingRegisters[Address + j] & 0xFF;
		 j++;
	}
	
	crc = CalcCRC(response, 3 + RegCount * 2);
	printf("outgoing crc = 0x%x\n", crc);
	
	response[3 + RegCount * 2] = crc & 0xFF ;
	response[3 + RegCount * 2 + 1] = (crc >> 8) & 0xFF;		
	
	return 3 + RegCount * 2 + 2;
}

static uint16_t CalcCRC(uint8_t *buf, int size)
{
	 uint16_t crc = 65535;

	for (int i = 0 ; i < size ; i++)
	{
		byte tableIndex = (byte) (crc ^ buf[i]);
		crc >>= 8;
		crc ^= crcTable[tableIndex];
	}
	
	return crc;
}
		
int MB_Apply_AssignDiagnostic(uint8_t *frame)
{
	return 0;
}	
	
/*
6.5 05 (0x05) Write Single Coil
This function code is used to write a single output to either ON or OFF in a remote device.
The requested ON/OFF state is specified by a constant in the request data field. A value of FF
00 hex requests the output to be ON. A value of 00 00 requests it to be OFF. All other values
are illegal and will not affect the output.
The Request PDU specifies the address of the coil to be forced. Coils are addressed starting
at zero. Therefore coil numbered 1 is addressed as 0. The requested ON/OFF state is
specified by a constant in the Coil Value field. A value of 0XFF00 requests the coil to be ON.
A value of 0X0000 requests the coil to be off. All other values are illegal and will not affect the
coil.

return the number of bytes to response back 
*/
int MB_Apply_WriteSingleCoil(uint8_t *frame)	
{
	#pragma pack(push, 1)
	typedef struct 
	{
		byte SlaveAddress;
		byte Function;
		byte OutputAddressHi;
	  byte OutputAddressLo;
	  byte OutputValueHi;
	  byte OutputValueLo;
	  byte CRCLo;
	  byte CRCHi;		
		
	} ST_WSingleCoil; 
	#pragma pack(pop)
	
	ST_WSingleCoil  *s = (ST_WSingleCoil *) frame;
	
	uint16_t Address = (s->OutputAddressHi << 8) | s->OutputAddressLo;
	uint16_t Value =   (s->OutputValueHi << 8)   |  s->OutputValueLo;
	uint16_t txCRC =     (s->CRCHi << 8)   |  s->CRCLo;
	
	uint16_t crc = CalcCRC(frame, 6);
	printf("incoming crc = 0x%x\n", crc);
	if (crc != txCRC)
	{
		 printf("incorrect incoming CRC %x  %x\n" , txCRC, crc);
	}
	
	Modbus_WriteSingleCoil(Address, Value);
	
	//The normal response is an echo of the request, returned after the coil state has been written.

	
	crc = CalcCRC(frame, 6);
	printf("outgoing crc = 0x%x\n", crc);
	memcpy(response , frame , 6);	
	
	response[6] = crc & 0xFF ;
	response[7] = (crc >> 8) & 0xFF;
	
	return 8;
}
int MB_Apply_WriteSingleRegister(uint8_t *frame)
{
	 	
		#pragma pack(push, 1)
		typedef struct 
		{
			byte SlaveAddress;
			byte Function;
			byte OutputAddressHi;
			byte OutputAddressLo;
			byte RegisterValueHi;
			byte RegisterValueLo;
	    byte CRCLo;			
			byte CRCHi;						
			
		} ST_WSingleReg; 
		#pragma pack(pop)
		
		ST_WSingleReg  *s = (ST_WSingleReg *) (frame);
		
		uint16_t Address = (s->OutputAddressHi << 8) | s->OutputAddressLo;
		uint16_t Value =   (s->RegisterValueHi << 8) | s->RegisterValueLo;
	  uint16_t txCRC =   (s->CRCHi << 8)   |  s->CRCLo;
	
		uint16_t crc = CalcCRC(frame, 6);
		printf("incoming crc = 0x%x\n", crc);
		if (crc != txCRC)
		{	
			printf("incorrect incoming CRC %x  %x\n" , txCRC, crc);
		}		
				
		Modbus_WriteSingleRegister(Address, Value);

		
			
		// Write Single Register Here..		
		//The normal response is an echo of the request, returned after the coil state has been written.		
		memcpy(response , frame , 6);	
		
	  crc = CalcCRC(frame, 6);
	  printf("outgoing crc = 0x%x\n", crc);
	
	  response[6] = crc & 0xFF ;
	  response[7] = (crc >> 8) & 0xFF;		
		
		return 8;
}

int MB_Apply_WriteMultipleRegisters(uint8_t *frame)
{
		 
		printf("MB_Apply_WriteMultipleRegisters\n");
	
		#pragma pack(push, 1)
		typedef struct 
		{
			byte SlaveAddress;
			byte Function;
			byte WriteStartingAddresHi;
			byte WriteStartingAddresLo;
			byte QuantityWriteHi;
			byte QuantityWriteLo;			
			byte WriteByteCount;
			
		} ST_WRMultiRegs; 
		#pragma pack(pop)

	

		ST_WRMultiRegs  *s = (ST_WRMultiRegs *) frame;		

		uint16_t WriteStartingAddres = (s->WriteStartingAddresHi << 8)   |  s->WriteStartingAddresLo;	
		uint16_t QuantityWrite = (s->QuantityWriteHi << 8)   |  s->QuantityWriteLo;	
				
		uint8_t *pData = (frame + sizeof(ST_WRMultiRegs));
		uint8_t *pCrcData = (frame + sizeof(ST_WRMultiRegs) + s->WriteByteCount);
		
	  uint16_t txCRC =   (*(pCrcData + 1) << 8)   |  *pCrcData;		
		printf("txCRC = 0x%x\n" , txCRC);
	
		uint16_t crc = CalcCRC(frame, sizeof(ST_WRMultiRegs) + s->WriteByteCount);
		printf("incoming crc = 0x%x\n", crc);
		if (crc != txCRC)
		{	
			  printf("incorrect incoming CRC %x  %x\n" , txCRC, crc);
		}				

		// Write Single Register Here..		
		//The normal response is an echo of the request, returned after the coil state has been written.		
		
	
		int j = 0;
		for (int i = 0; i < QuantityWrite * 2; i+=2)
		{
			  DataStore_InputRegisters[WriteStartingAddres + j + i] = *(pData + i);
  			DataStore_InputRegisters[WriteStartingAddres + j + i + 1] = *(pData + i + 1);
		}

		memcpy(response , frame , 6);		
	  crc = CalcCRC(response, 6);
	  printf("outgoing crc = 0x%x\n", crc);
		
	
	  response[6] = crc & 0xFF ;
	  response[7] = (crc >> 8) & 0xFF;		
		
		return 8;
		
}


// follow   in http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf 
int MB_Apply_ReadWriteMultipleRegisters(uint8_t *frame)
{
		 
		printf("MB_Apply_ReadWriteMultipleRegisters\n");
		

		ST_WRwMultiRegs  *s = (ST_WRwMultiRegs *) frame;		
		uint16_t ReadStartingAddress = (s->ReadStartingAddresHi << 8) | s->ReadStartingAddresLo;
		uint16_t QuantityRead = (s->QuantityReadHi << 8) | s->QuantityReadLo;
	  printf("QuantityRead = %d\n", QuantityRead);
	
				
		uint8_t *pData = (frame + sizeof(ST_WRwMultiRegs));
		uint8_t *pCrcData = (frame + sizeof(ST_WRwMultiRegs) + s->WriteByteCount);
		
	  uint16_t txCRC =   (*(pCrcData + 1) << 8)   |  *pCrcData;		
		printf("txCRC = 0x%x\n" , txCRC);
	
		uint16_t crc = CalcCRC(frame, sizeof(ST_WRwMultiRegs) + s->WriteByteCount);
		printf("incoming crc = 0x%x\n", crc);
		if (crc != txCRC)
		{	
			  printf("incorrect incoming CRC %x  %x\n" , txCRC, crc);
		}				

		// Write Single Register Here..		
		//The normal response is an echo of the request, returned after the coil state has been written.		
		
		response[0] = frame[0];
		response[1] = frame[1];
		response[2] = QuantityRead;
		
		int j = 0;
		for (int i = 0; i < QuantityRead * 2; i+=2)
		{
			  response[3 + i] = DataStore_InputRegisters[ReadStartingAddress + j + i];
  			response[3 + i + 1] = DataStore_InputRegisters[ReadStartingAddress + j + i + 1];
		}
		
	  crc = CalcCRC(response, 3 + QuantityRead * 2);
	  printf("outgoing crc = 0x%x\n", crc);
	
	  response[3 + QuantityRead * 2] = crc & 0xFF ;
	  response[3 + QuantityRead * 2 + 1] = (crc >> 8) & 0xFF;		
		
		return 3 + QuantityRead * 2 + 2;
}

// follow 6.11 15 (0x0F) Write Multiple Coils  in http://www.modbus.org/docs/Modbus_Application_Protocol_V1_1b3.pdf 
int MB_Apply_WriteMultipleCoils(uint8_t *frame)
{
		
		#pragma pack(push, 1)
		typedef struct 
		{
			byte SlaveAddress;
			byte Function;
			byte StartingAddressHi;
		  byte StartingAddressLo;
			byte QuantityOfOutputsHi;
		  byte QuantityOfOutputsLo;
			byte ByteCount;
		} ST_WMultiCoil; 
		#pragma pack(pop)
		
		printf("MB_Apply_WriteMultipleCoils\n");
		

		ST_WMultiCoil  *s = (ST_WMultiCoil *) frame;		
		uint16_t StartingAddress = (s->StartingAddressHi << 8) | s->StartingAddressLo;
		uint16_t QuantityOfOutputs = (s->QuantityOfOutputsHi << 8) | s->QuantityOfOutputsLo;
				
		uint8_t *pData = (frame + sizeof(ST_WMultiCoil) + s->ByteCount);
	  uint16_t txCRC =   (*(pData + 1) << 8)   |  *pData;		
		printf("txCRC = 0x%x\n" , txCRC);
	
		uint16_t crc = CalcCRC(frame, sizeof(ST_WMultiCoil) + s->ByteCount);
		printf("incoming crc = 0x%x\n", crc);
		if (crc != txCRC)
		{	
			  printf("incorrect incoming CRC %x  %x\n" , txCRC, crc);
		}		
		
		printf("s->ByteCount = %d\n", s->ByteCount);
		
		byte b = 0;
		uint16_t address = StartingAddress;
		for (int i = 0; i < s->ByteCount ; i++)
		{
			if ( i == 0)
			{
				 b = frame[s->ByteCount + sizeof(ST_WMultiCoil) + i];
				 printf("b = 0x%x\n", b);
				 for (int j = 0 ; j < QuantityOfOutputs; j++)
				 { 				 
						if (b & 0x80)
						{
								 // Coil Address = StartingAddress  
								 Modbus_WriteSingleCoil(address, 1);
						} 
						else 
						{
								 // Coil Address = StartingAddress
								 Modbus_WriteSingleCoil(address, 0);
						}
						b = b << 1; 
						address+=1;
 				 }	
	  	}			 
			if (i == 1)
			{
				 b = frame[s->ByteCount + sizeof(ST_WMultiCoil) + i];
				 for (int j = 0 ; j < QuantityOfOutputs; j++)
				 { 				 
						if (b & 0x1)
						{
								 // Coil Address = StartingAddress  
								 Modbus_WriteSingleCoil(address, 1);
						} 
						else 
						{
								 Modbus_WriteSingleCoil(address, 0);
						}
						b = b >> 1; 
						address+=1;
 				 }																
			}
		}
		
			// Write Single Register Here..		
		//The normal response is an echo of the request, returned after the coil state has been written.		
		memcpy(response , frame , 6);	
		
	  crc = CalcCRC(frame, 6);
	  printf("outgoing crc = 0x%x\n", crc);
	
	  response[6] = crc & 0xFF ;
	  response[7] = (crc >> 8) & 0xFF;		
		
		return 8;
}
