/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "stdio.h"
#include "usart.h"
#include <stdint.h>
#include "gpio.h"
#include "modbus_slave.h"
#include "modbus_funcs.h"


const int RequestFrameStartLength = 7;
const int ResponseFrameStartLength = 4;
extern UART_HandleTypeDef huart1;

static uint8_t frame[MAX_MB_SLAVE_FRAME_BUFFER];
static uint32_t m_mbFrameSize = 0;
static uint32_t m_unitId = 1;
const int MinRequestFrameLength = 3;
extern uint8_t response[MAX_MB_SLAVE_FRAME_BUFFER];
 

// Static functions 
static int Modbus_CreateModbusRequest(byte functionCode);



void Modbus_SetUnitId(uint32_t uid)
{
	m_unitId = uid;
}

static int Modbus_RequestBytesToRead(void)
{
	byte functionCode = frame[1];
	int numBytes;
	
	switch (functionCode)
	{
		case MB_ReadCoils:
		case MB_ReadInputs:
		case MB_ReadHoldingRegisters:
		case MB_ReadInputRegisters:
		case MB_WriteSingleCoil:
		case MB_WriteSingleRegister:
		case MB_Diagnostics:
			numBytes = 1;
		  m_mbFrameSize = RequestFrameStartLength + numBytes;
		break;
		case MB_WriteMultipleCoils:
		case MB_WriteMultipleRegisters:
		{
			 byte byteCount = frame[6];
			 numBytes = byteCount + 2;
			 m_mbFrameSize = RequestFrameStartLength + numBytes;
		}		
	  break;
		case MB_ReadWriteMultipleRegisters:
		{
		   if (HAL_UART_Receive(&huart1, frame + 7, 4, 1000) != HAL_OK)
		   {
		      return -1;
			 }
			 byte byteCount = frame[10];
			 numBytes = byteCount +  2;  
			 m_mbFrameSize = 11 + numBytes;
		   if (HAL_UART_Receive(&huart1, frame + 11, numBytes, 1000) != HAL_OK)
		   {
		      return -1;
			 }			 
			 numBytes = 0; // because we already read all the frame
		}
		break;
		default:
			printf("function not supported: %d\n", functionCode);
		break;
	}

	return numBytes;
}

static int Modbus_CreateModbusRequest(byte functionCode)
{

	if (m_mbFrameSize < MinRequestFrameLength)
	{
		  return 0;
	} 
	
	switch (functionCode)
	{
		case MB_ReadCoils:
		case MB_ReadInputs:
				//request = CreateModbusMessage<ReadCoilsInputsRequest>(frame);
		break;
		case MB_ReadHoldingRegisters:
			return MB_Apply_ReadHoldingRegisters(frame);			
		case MB_ReadInputRegisters:
			  return MB_Apply_ReadInputRegisters(frame);			
		case MB_WriteSingleCoil:
				 return MB_Apply_WriteSingleCoil(frame);
		case MB_WriteSingleRegister:
				return MB_Apply_WriteSingleRegister(frame);
		case MB_Diagnostics:
			//request = CreateModbusMessage<DiagnosticsRequestResponse>(frame);
			break;
		case MB_WriteMultipleCoils:
				 return MB_Apply_WriteMultipleCoils(frame);
		case MB_WriteMultipleRegisters:
			    return MB_Apply_WriteMultipleRegisters(frame);
			break;
		case MB_ReadWriteMultipleRegisters:
				return MB_Apply_ReadWriteMultipleRegisters(frame);
		default:
		  printf( "Unsupported function code %d", functionCode);
	}

	return MB_OK;
  
}
 
MB_ERRORS Modbus_ReadRequest(uint32_t timeOut)
{
	
	byte SlaveAddress;
  byte FunctionCode;

	
	//byte[] frameStart = Read(ResponseFrameStartLength);
	// Read the first 7 bytes
	if (HAL_UART_Receive(&huart1, frame, RequestFrameStartLength, timeOut) != HAL_OK)
	{
		 return MB_FRAME_TIMEOUT;
	}
	int numBytes = Modbus_RequestBytesToRead();
	if (numBytes == -1)
	{
		  return MB_FRAME_TIMEOUT;
	}
	if ((numBytes + RequestFrameStartLength) > MAX_MB_SLAVE_FRAME_BUFFER)
	{		
		return MB_INTERNAL_BUFFER_OVERFLOW;
	}

	if (numBytes > 0)
	{
		if (HAL_UART_Receive(&huart1, frame + RequestFrameStartLength,numBytes, timeOut) != HAL_OK)
		{
			 printf("not received 7 bytes");
			 return MB_NOT_ENOUGH_DATA;
		}
	}
	printf("m_mbFrameSize: %d\n" , m_mbFrameSize);
	 
	SlaveAddress = frame[0]; 
	printf("Slave Address %d\n", SlaveAddress);
	if (SlaveAddress != m_unitId)
	{
		 printf("NModbus Slave %d ignoring request intended for NModbus Slave %d ", m_unitId, SlaveAddress);
		 return MB_OK;
	}
	
  FunctionCode = frame[1];
	printf("Function Code: %d\n", FunctionCode);
	int numToSend = Modbus_CreateModbusRequest(FunctionCode);
	if (HAL_UART_Transmit(&huart1, response , numToSend, timeOut) != HAL_OK)
	{
		 return MB_FAILED_TO_TRAMSMIT_UART_DATA;
	}
	
	printf("Send ok %d byte\n", numToSend);
	return MB_OK;
}
