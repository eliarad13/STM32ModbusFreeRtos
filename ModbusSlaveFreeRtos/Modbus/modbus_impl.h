#ifndef _MODBUS_IMPLEM
#define _MODBUS_IMPLEM

#include <stdint.h>

void Modbus_WriteSingleRegister(uint16_t startAddress, uint16_t value);
void	Modbus_WriteSingleCoil(uint16_t startAddress, uint16_t value); 
#endif 

