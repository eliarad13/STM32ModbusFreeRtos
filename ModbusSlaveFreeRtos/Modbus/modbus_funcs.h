
#ifndef _MODBUS_FUNCS_H
#define _MODBUS_FUNCS_H

#include <stdint.h>
#include "common.h"



#pragma pack(push, 1)
typedef struct 
{
	byte SlaveAddress;
	byte Function;
	byte ReadStartingAddresHi;
	byte ReadStartingAddresLo;
	byte QuantityReadHi;
	byte QuantityReadLo;
	byte WriteStartingAddresHi;
	byte WriteStartingAddresLo;
	byte QuantityWriteHi;
	byte QuantityWriteLo;			
	byte WriteByteCount;
	
} ST_WRwMultiRegs; 
#pragma pack(pop)










int MB_Apply_ReadCoilDiscretes(uint8_t *frame);
int MB_Apply_ReadInputDiscretes(uint8_t *frame);
int MB_Apply_ReadHoldingRegisters(uint8_t *frame);
int MB_Apply_ReadInputRegisters(uint8_t *frame);
int MB_Apply_AssignDiagnostic(uint8_t *frame);
int MB_Apply_WriteSingleCoil(uint8_t *frame);
int MB_Apply_WriteSingleRegister(uint8_t *frame);
int MB_Apply_WriteMultipleCoils(uint8_t *frame);
int MB_Apply_ReadWriteMultipleRegisters(uint8_t *frame);
void MB_DataStoreInit(void);


#endif
