#ifndef _MODBUS_SLAVE_H
#define _MODBUS_SLAVE_H


#include "common.h"


#define MAX_MB_SLAVE_FRAME_BUFFER 1000

typedef enum 
{
		// supported function codes
		MB_ReadCoils = 1,
		MB_ReadInputs = 2,
		MB_ReadHoldingRegisters = 3,
		MB_ReadInputRegisters = 4,
		MB_WriteSingleCoil = 5,
		MB_WriteSingleRegister = 6,
		MB_Diagnostics = 8,
		MB_DiagnosticsReturnQueryData = 0,
		MB_WriteMultipleCoils = 15,
		MB_WriteMultipleRegisters = 16,
		MB_ReadWriteMultipleRegisters = 23,

		MB_MaximumDiscreteRequestResponseSize = 2040,
		MB_MaximumRegisterRequestResponseSize = 127,

		// modbus slave exception offset that is added to the function code, to flag an exception
		MB_ExceptionOffset = 128,

		// modbus slave exception codes
		MB_Acknowledge = 5,
		MB_SlaveDeviceBusy = 6,

		// default setting for number of retries for IO operations
		MB_DefaultRetries = 3,

		// default number of milliseconds to wait after encountering an ACKNOWLEGE or SLAVE DEVIC BUSY slave exception response.
		MB_DefaultWaitToRetryMilliseconds = 250,

		// default setting for IO timeouts in milliseconds
		MB_DefaultTimeout = 1000,

		// smallest supported message frame size (sans checksum)
		MB_MinimumFrameSize = 2,

		MB_CoilOn = 0xFF00,
		MB_CoilOff = 0x0000,
	
		// IP slaves should be addressed by IP
		MB_DefaultIpSlaveUnitId = 0,

		// An existing connection was forcibly closed by the remote host
		MB_ConnectionResetByPeer = 10054,
		
		// Existing socket connection is being closed
		MB_WSACancelBlockingCall = 10004

	
} MODBUS_DEFS;


typedef enum 
{
	MB_INTERNAL_BUFFER_OVERFLOW,	
	MB_OK,
	MB_FRAME_TIMEOUT,
	MB_NOT_ENOUGH_DATA,
	MIN_MB_REQUEST_LESS_3,
	MB_FAILED_TO_TRAMSMIT_UART_DATA
	
} MB_ERRORS;
	
MB_ERRORS Modbus_ReadRequest(uint32_t timeOut);


#endif 
