﻿namespace MasterApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpen = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.chkSimulateSlave = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtMultiCoilsValues = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMasterComPort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMultRegs = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.chkSingleCoil = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSlaveComPort = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHoldingRead = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtReadHolding = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtWriteMultipleCoils = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSingleCoilAddress = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtWriteMultipleRegs = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtInputRegsAmount = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtInputRegsAddress = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnInputRead = new System.Windows.Forms.Button();
            this.txtInputRegsResult = new System.Windows.Forms.TextBox();
            this.txtReadHoldingAmount = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtWriteSingleRegAddress = new System.Windows.Forms.TextBox();
            this.btnWriteSingleRegister = new System.Windows.Forms.Button();
            this.txtSingleRegValue = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtRWWriteData = new System.Windows.Forms.TextBox();
            this.txtRWReadAmount = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtRWReadAddress = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtRWWriteAmount = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtRWWriteAddress = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnPerformReadWrite = new System.Windows.Forms.Button();
            this.txtRWReadResult = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(28, 103);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(75, 23);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(340, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Modbus Master RTU Serial";
            // 
            // chkSimulateSlave
            // 
            this.chkSimulateSlave.AutoSize = true;
            this.chkSimulateSlave.Location = new System.Drawing.Point(18, 515);
            this.chkSimulateSlave.Name = "chkSimulateSlave";
            this.chkSimulateSlave.Size = new System.Drawing.Size(276, 17);
            this.chkSimulateSlave.TabIndex = 3;
            this.chkSimulateSlave.Text = "Simulate slave in pc ( need cros cable from pc to pc )";
            this.chkSimulateSlave.UseVisualStyleBackColor = true;
            this.chkSimulateSlave.CheckedChanged += new System.EventHandler(this.chkSimulateSlave_CheckedChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(461, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Write";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtMultiCoilsValues
            // 
            this.txtMultiCoilsValues.Location = new System.Drawing.Point(263, 45);
            this.txtMultiCoilsValues.Name = "txtMultiCoilsValues";
            this.txtMultiCoilsValues.Size = new System.Drawing.Size(71, 20);
            this.txtMultiCoilsValues.TabIndex = 5;
            this.txtMultiCoilsValues.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(253, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Write multiple coils:";
            // 
            // txtMasterComPort
            // 
            this.txtMasterComPort.Location = new System.Drawing.Point(129, 103);
            this.txtMasterComPort.Name = "txtMasterComPort";
            this.txtMasterComPort.Size = new System.Drawing.Size(67, 20);
            this.txtMasterComPort.TabIndex = 7;
            this.txtMasterComPort.Text = "COM11";
            this.txtMasterComPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Write single coil:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(319, 300);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Write multiple registers:";
            // 
            // txtMultRegs
            // 
            this.txtMultRegs.Location = new System.Drawing.Point(330, 317);
            this.txtMultRegs.Name = "txtMultRegs";
            this.txtMultRegs.Size = new System.Drawing.Size(71, 20);
            this.txtMultRegs.TabIndex = 12;
            this.txtMultRegs.Text = "55,23,12";
            this.txtMultRegs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(528, 318);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Write";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // chkSingleCoil
            // 
            this.chkSingleCoil.AutoSize = true;
            this.chkSingleCoil.Location = new System.Drawing.Point(217, 45);
            this.chkSingleCoil.Name = "chkSingleCoil";
            this.chkSingleCoil.Size = new System.Drawing.Size(15, 14);
            this.chkSingleCoil.TabIndex = 14;
            this.chkSingleCoil.UseVisualStyleBackColor = true;
            this.chkSingleCoil.CheckedChanged += new System.EventHandler(this.chkSingleCoil_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(129, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Com Port";
            // 
            // txtSlaveComPort
            // 
            this.txtSlaveComPort.Location = new System.Drawing.Point(309, 513);
            this.txtSlaveComPort.Name = "txtSlaveComPort";
            this.txtSlaveComPort.Size = new System.Drawing.Size(67, 20);
            this.txtSlaveComPort.TabIndex = 18;
            this.txtSlaveComPort.Text = "COM12";
            this.txtSlaveComPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(257, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Read holding";
            // 
            // txtHoldingRead
            // 
            this.txtHoldingRead.Location = new System.Drawing.Point(256, 108);
            this.txtHoldingRead.Name = "txtHoldingRead";
            this.txtHoldingRead.Size = new System.Drawing.Size(280, 20);
            this.txtHoldingRead.TabIndex = 20;
            this.txtHoldingRead.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(180, 105);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Read";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // txtReadHolding
            // 
            this.txtReadHolding.Location = new System.Drawing.Point(20, 108);
            this.txtReadHolding.Name = "txtReadHolding";
            this.txtReadHolding.Size = new System.Drawing.Size(71, 20);
            this.txtReadHolding.TabIndex = 22;
            this.txtReadHolding.Text = "1";
            this.txtReadHolding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Address";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(369, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "Address";
            // 
            // txtWriteMultipleCoils
            // 
            this.txtWriteMultipleCoils.Location = new System.Drawing.Point(368, 45);
            this.txtWriteMultipleCoils.Name = "txtWriteMultipleCoils";
            this.txtWriteMultipleCoils.Size = new System.Drawing.Size(71, 20);
            this.txtWriteMultipleCoils.TabIndex = 24;
            this.txtWriteMultipleCoils.Text = "1";
            this.txtWriteMultipleCoils.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(125, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Address";
            // 
            // txtSingleCoilAddress
            // 
            this.txtSingleCoilAddress.Location = new System.Drawing.Point(124, 43);
            this.txtSingleCoilAddress.Name = "txtSingleCoilAddress";
            this.txtSingleCoilAddress.Size = new System.Drawing.Size(71, 20);
            this.txtSingleCoilAddress.TabIndex = 26;
            this.txtSingleCoilAddress.Text = "1";
            this.txtSingleCoilAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(444, 303);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Address";
            // 
            // txtWriteMultipleRegs
            // 
            this.txtWriteMultipleRegs.Location = new System.Drawing.Point(443, 320);
            this.txtWriteMultipleRegs.Name = "txtWriteMultipleRegs";
            this.txtWriteMultipleRegs.Size = new System.Drawing.Size(71, 20);
            this.txtWriteMultipleRegs.TabIndex = 28;
            this.txtWriteMultipleRegs.Text = "1";
            this.txtWriteMultipleRegs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtInputRegsAmount);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtInputRegsAddress);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.btnInputRead);
            this.groupBox1.Controls.Add(this.txtInputRegsResult);
            this.groupBox1.Controls.Add(this.txtReadHoldingAmount);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtWriteSingleRegAddress);
            this.groupBox1.Controls.Add(this.btnWriteSingleRegister);
            this.groupBox1.Controls.Add(this.txtSingleRegValue);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtReadHolding);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.txtWriteMultipleRegs);
            this.groupBox1.Controls.Add(this.txtMultiCoilsValues);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtSingleCoilAddress);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.txtWriteMultipleCoils);
            this.groupBox1.Controls.Add(this.txtMultRegs);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.chkSingleCoil);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtHoldingRead);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(26, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(638, 364);
            this.groupBox1.TabIndex = 30;
            this.groupBox1.TabStop = false;
            // 
            // txtInputRegsAmount
            // 
            this.txtInputRegsAmount.Location = new System.Drawing.Point(103, 180);
            this.txtInputRegsAmount.Name = "txtInputRegsAmount";
            this.txtInputRegsAmount.Size = new System.Drawing.Size(71, 20);
            this.txtInputRegsAmount.TabIndex = 42;
            this.txtInputRegsAmount.Text = "1";
            this.txtInputRegsAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(104, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 43;
            this.label14.Text = "Amount";
            // 
            // txtInputRegsAddress
            // 
            this.txtInputRegsAddress.Location = new System.Drawing.Point(24, 180);
            this.txtInputRegsAddress.Name = "txtInputRegsAddress";
            this.txtInputRegsAddress.Size = new System.Drawing.Size(71, 20);
            this.txtInputRegsAddress.TabIndex = 40;
            this.txtInputRegsAddress.Text = "1";
            this.txtInputRegsAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 164);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "Address";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(261, 164);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "Read Input";
            // 
            // btnInputRead
            // 
            this.btnInputRead.Location = new System.Drawing.Point(184, 177);
            this.btnInputRead.Name = "btnInputRead";
            this.btnInputRead.Size = new System.Drawing.Size(68, 23);
            this.btnInputRead.TabIndex = 37;
            this.btnInputRead.Text = "Read";
            this.btnInputRead.UseVisualStyleBackColor = true;
            this.btnInputRead.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtInputRegsResult
            // 
            this.txtInputRegsResult.Location = new System.Drawing.Point(260, 180);
            this.txtInputRegsResult.Name = "txtInputRegsResult";
            this.txtInputRegsResult.Size = new System.Drawing.Size(280, 20);
            this.txtInputRegsResult.TabIndex = 38;
            this.txtInputRegsResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtReadHoldingAmount
            // 
            this.txtReadHoldingAmount.Location = new System.Drawing.Point(99, 108);
            this.txtReadHoldingAmount.Name = "txtReadHoldingAmount";
            this.txtReadHoldingAmount.Size = new System.Drawing.Size(71, 20);
            this.txtReadHoldingAmount.TabIndex = 35;
            this.txtReadHoldingAmount.Text = "1";
            this.txtReadHoldingAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(100, 92);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "Amount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(139, 303);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Address";
            // 
            // txtWriteSingleRegAddress
            // 
            this.txtWriteSingleRegAddress.Location = new System.Drawing.Point(138, 320);
            this.txtWriteSingleRegAddress.Name = "txtWriteSingleRegAddress";
            this.txtWriteSingleRegAddress.Size = new System.Drawing.Size(71, 20);
            this.txtWriteSingleRegAddress.TabIndex = 33;
            this.txtWriteSingleRegAddress.Text = "1";
            this.txtWriteSingleRegAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnWriteSingleRegister
            // 
            this.btnWriteSingleRegister.Location = new System.Drawing.Point(223, 318);
            this.btnWriteSingleRegister.Name = "btnWriteSingleRegister";
            this.btnWriteSingleRegister.Size = new System.Drawing.Size(75, 23);
            this.btnWriteSingleRegister.TabIndex = 30;
            this.btnWriteSingleRegister.Text = "Write";
            this.btnWriteSingleRegister.UseVisualStyleBackColor = true;
            this.btnWriteSingleRegister.Click += new System.EventHandler(this.btnWriteSingleRegister_Click);
            // 
            // txtSingleRegValue
            // 
            this.txtSingleRegValue.Location = new System.Drawing.Point(25, 317);
            this.txtSingleRegValue.Name = "txtSingleRegValue";
            this.txtSingleRegValue.Size = new System.Drawing.Size(71, 20);
            this.txtSingleRegValue.TabIndex = 31;
            this.txtSingleRegValue.Text = "35";
            this.txtSingleRegValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(14, 300);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Write single register:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtRWWriteData);
            this.groupBox2.Controls.Add(this.txtRWReadAmount);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtRWReadAddress);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.txtRWWriteAmount);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtRWWriteAddress);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.btnPerformReadWrite);
            this.groupBox2.Controls.Add(this.txtRWReadResult);
            this.groupBox2.Location = new System.Drawing.Point(671, 132);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(510, 364);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Read/Write Multiple registers";
            // 
            // txtRWWriteData
            // 
            this.txtRWWriteData.Location = new System.Drawing.Point(194, 52);
            this.txtRWWriteData.Name = "txtRWWriteData";
            this.txtRWWriteData.Size = new System.Drawing.Size(280, 20);
            this.txtRWWriteData.TabIndex = 44;
            this.txtRWWriteData.Text = "22,11,55";
            this.txtRWWriteData.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtRWReadAmount
            // 
            this.txtRWReadAmount.Location = new System.Drawing.Point(94, 134);
            this.txtRWReadAmount.Name = "txtRWReadAmount";
            this.txtRWReadAmount.Size = new System.Drawing.Size(71, 20);
            this.txtRWReadAmount.TabIndex = 53;
            this.txtRWReadAmount.Text = "3";
            this.txtRWReadAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(95, 118);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 54;
            this.label20.Text = "Amount";
            // 
            // txtRWReadAddress
            // 
            this.txtRWReadAddress.Location = new System.Drawing.Point(15, 134);
            this.txtRWReadAddress.Name = "txtRWReadAddress";
            this.txtRWReadAddress.Size = new System.Drawing.Size(71, 20);
            this.txtRWReadAddress.TabIndex = 51;
            this.txtRWReadAddress.Text = "1";
            this.txtRWReadAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 118);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(69, 13);
            this.label21.TabIndex = 52;
            this.label21.Text = "Address read";
            // 
            // txtRWWriteAmount
            // 
            this.txtRWWriteAmount.Location = new System.Drawing.Point(90, 52);
            this.txtRWWriteAmount.Name = "txtRWWriteAmount";
            this.txtRWWriteAmount.Size = new System.Drawing.Size(71, 20);
            this.txtRWWriteAmount.TabIndex = 49;
            this.txtRWWriteAmount.Text = "3";
            this.txtRWWriteAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(91, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Amount";
            // 
            // txtRWWriteAddress
            // 
            this.txtRWWriteAddress.Location = new System.Drawing.Point(11, 52);
            this.txtRWWriteAddress.Name = "txtRWWriteAddress";
            this.txtRWWriteAddress.Size = new System.Drawing.Size(71, 20);
            this.txtRWWriteAddress.TabIndex = 47;
            this.txtRWWriteAddress.Text = "1";
            this.txtRWWriteAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 36);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 13);
            this.label18.TabIndex = 48;
            this.label18.Text = "Address write";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(195, 121);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 46;
            this.label19.Text = "Read Input";
            // 
            // btnPerformReadWrite
            // 
            this.btnPerformReadWrite.Location = new System.Drawing.Point(111, 249);
            this.btnPerformReadWrite.Name = "btnPerformReadWrite";
            this.btnPerformReadWrite.Size = new System.Drawing.Size(245, 43);
            this.btnPerformReadWrite.TabIndex = 44;
            this.btnPerformReadWrite.Text = "Perform";
            this.btnPerformReadWrite.UseVisualStyleBackColor = true;
            this.btnPerformReadWrite.Click += new System.EventHandler(this.btnPerformReadWrite_Click);
            // 
            // txtRWReadResult
            // 
            this.txtRWReadResult.Location = new System.Drawing.Point(194, 137);
            this.txtRWReadResult.Multiline = true;
            this.txtRWReadResult.Name = "txtRWReadResult";
            this.txtRWReadResult.Size = new System.Drawing.Size(221, 77);
            this.txtRWReadResult.TabIndex = 45;
            this.txtRWReadResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 545);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtSlaveComPort);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtMasterComPort);
            this.Controls.Add(this.chkSimulateSlave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOpen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modbus Tester application";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkSimulateSlave;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtMultiCoilsValues;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMasterComPort;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMultRegs;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox chkSingleCoil;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSlaveComPort;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHoldingRead;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtReadHolding;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtWriteMultipleCoils;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSingleCoilAddress;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtWriteMultipleRegs;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWriteSingleRegAddress;
        private System.Windows.Forms.Button btnWriteSingleRegister;
        private System.Windows.Forms.TextBox txtSingleRegValue;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtReadHoldingAmount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtInputRegsAmount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtInputRegsAddress;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnInputRead;
        private System.Windows.Forms.TextBox txtInputRegsResult;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtRWWriteAmount;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtRWWriteAddress;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnPerformReadWrite;
        private System.Windows.Forms.TextBox txtRWReadResult;
        private System.Windows.Forms.TextBox txtRWReadAmount;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtRWReadAddress;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtRWWriteData;
    }
}

