﻿using FtdAdapter;
using Modbus.Data;
using Modbus.Device;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterApp
{
    public partial class Form1 : Form
    {

        SerialPort m_masterPort;
        IModbusSerialMaster m_master;
        Task m_slaveTask;
        ushort m_startAddress = 1;
        byte m_slaveAddress = 1;

        public Form1()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            txtMasterComPort.Text = Properties.Settings.Default.ComMaster;
            txtSlaveComPort.Text = Properties.Settings.Default.ComSlave;
        }

        public void StartModbusSerialRtuSlave()
        {
            string comPort = txtSlaveComPort.Text;

            using (SerialPort slavePort = new SerialPort(comPort))
            {
                try
                {
                    // configure serial port
                    slavePort.BaudRate = 9600;
                    slavePort.DataBits = 8;
                    slavePort.Parity = Parity.None;
                    slavePort.StopBits = StopBits.One;
                    slavePort.Open();

                    byte unitId = 1;
                    // create modbus slave
                    ModbusSlave slave = ModbusSerialSlave.CreateRtu(unitId, slavePort);
                    slave.DataStore = DataStoreFactory.CreateDefaultDataStore();
                    chkSimulateSlave.ForeColor = Color.Green;
                    slave.Listen();
                  
                }
                catch (Exception err)
                {
                    chkSimulateSlave.ForeColor = Color.Red;
                    MessageBox.Show(err.Message);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_master == null)
                {
                    m_masterPort = new SerialPort(txtMasterComPort.Text);
                    // configure usb port
                    m_masterPort.BaudRate = 115200;
                    m_masterPort.DataBits = 8;
                    m_masterPort.Parity = Parity.None;
                    m_masterPort.StopBits = StopBits.One;
                    m_masterPort.Open();
                    m_masterPort.ReadTimeout = 3000;
                    m_masterPort.WriteTimeout = 3000;
                    btnOpen.ForeColor = Color.Green;
                    // create modbus master
                    m_master = ModbusSerialMaster.CreateRtu(m_masterPort);
                    groupBox1.Enabled = true;
                }
                else
                {
                    m_masterPort.Close();
                    m_masterPort = null;
                    btnOpen.ForeColor = Color.Red;
                    groupBox1.Enabled = false;
                }
            }
            catch (Exception err)
            {
                btnOpen.ForeColor = Color.Red;
                MessageBox.Show("Failed to open: " + err.Message);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (m_master != null)
            {
                if (m_masterPort != null)
                    m_masterPort.Close();
            }
            Properties.Settings.Default.ComMaster = txtMasterComPort.Text;
            Properties.Settings.Default.ComSlave = txtSlaveComPort.Text;
            Properties.Settings.Default.Save();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_masterPort != null && m_masterPort.IsOpen == true)
                {
                    ushort[] registers;

                    string[] s = txtMultRegs.Text.Split(new Char[] { ',' });

                    registers = new ushort[s.Length];
                    for (int i = 0; i < registers.Length; i++)
                    {
                        registers[i] = ushort.Parse(s[i]);
                    }

                    // write three registers
                    ushort address = ushort.Parse(txtWriteMultipleRegs.Text);
                    m_master.WriteMultipleRegisters(m_slaveAddress, address, registers);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Failed:" + err.Message);
            }
        }

        private void chkSingleCoil_CheckedChanged(object sender, EventArgs e)
        {

            try
            {
                if (m_masterPort != null && m_masterPort.IsOpen == true)
                {
                    // write three registers
                    ushort address = ushort.Parse(txtSingleCoilAddress.Text);
                    m_master.WriteSingleCoil(m_slaveAddress, address, chkSingleCoil.Checked);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Failed:" + err.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_masterPort != null && m_masterPort.IsOpen == true)
                {
                    ushort address = ushort.Parse(txtWriteMultipleCoils.Text);
                    if (txtMultiCoilsValues.Text == string.Empty)
                        m_master.WriteMultipleCoils(m_slaveAddress, address, new bool[] { true, false, true, true, true, false });
                    else
                    {
                        MessageBox.Show("We always write true , false , true for demo purposes");
                        txtMultiCoilsValues.Text = string.Empty;
                        m_master.WriteMultipleCoils(m_slaveAddress, address, new bool[] { true, false, true });
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Failed:" + err.Message);
            }
        }

        private void chkSimulateSlave_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSimulateSlave.Checked == true)
            {
                m_slaveTask = new Task(() => {
                    StartModbusSerialRtuSlave();
                });
                m_slaveTask.Start();

              
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (m_masterPort != null && m_masterPort.IsOpen == true)
                {
                    ushort address = ushort.Parse(txtReadHolding.Text);
                    ushort amount = ushort.Parse(txtReadHoldingAmount.Text);
                    ushort[] holdingRegs = m_master.ReadHoldingRegisters(m_slaveAddress, address, amount);
                    txtHoldingRead.Clear();
                    for (int i = 0; i < holdingRegs.Length; i++)
                    {
                        if (i > 0)
                            txtHoldingRead.AppendText(",");
                        txtHoldingRead.AppendText(holdingRegs[i].ToString());
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Failed:" + err.Message);
            }
        }

        private void btnWriteSingleRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_masterPort != null && m_masterPort.IsOpen == true)
                {

                    ushort regValue = ushort.Parse(txtSingleRegValue.Text);

                    // write three registers
                    ushort address = ushort.Parse(txtWriteSingleRegAddress.Text);
                    m_master.WriteSingleRegister(m_slaveAddress, address, regValue);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Failed:" + err.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (m_masterPort != null && m_masterPort.IsOpen == true)
                {
                    ushort address = ushort.Parse(txtInputRegsAddress.Text);
                    ushort amount = ushort.Parse(txtInputRegsAmount.Text);
                    ushort[] holdingRegs = m_master.ReadInputRegisters(m_slaveAddress, address, amount);
                    txtInputRegsResult.Clear();
                    for (int i = 0; i < holdingRegs.Length; i++)
                    {
                        if (i > 0)
                            txtInputRegsResult.AppendText(",");
                        txtInputRegsResult.AppendText(holdingRegs[i].ToString());
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Failed:" + err.Message);
            }
        }

        private void btnPerformReadWrite_Click(object sender, EventArgs e)
        {
            try
            {
                ushort startReadAddress = ushort.Parse(txtRWReadAddress.Text);
                ushort numberOfPointsToRead = ushort.Parse(txtRWReadAmount.Text);
                ushort startWriteAddress = ushort.Parse(txtRWWriteAddress.Text);

                ushort[] writeData;
                string[] s = txtRWWriteData.Text.Split(new Char[] { ',' });

                writeData = new ushort[s.Length];
                for (int i = 0; i < writeData.Length; i++)
                {
                    writeData[i] = ushort.Parse(s[i]);
                }

                //ushort numberOfPointsToRead, ushort startWriteAddress, ushort[] writeData
                ushort [] rwRegsResult = m_master.ReadWriteMultipleRegisters(m_slaveAddress, startReadAddress, numberOfPointsToRead, startWriteAddress, writeData);

                txtRWReadResult.Clear();
                for (int i = 0; i < rwRegsResult.Length; i++)
                {
                    if (i > 0)
                        txtRWReadResult.AppendText(",");
                    txtRWReadResult.AppendText(rwRegsResult[i].ToString()+ Environment.NewLine);
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }
    }
}
